#include <cstdio>
#include <vector>
#include <iostream>
#include <signal.h>
#include "Headers/UDPServer.hpp"
#include "Headers/ConcurrentQueue.hpp"
#include "Headers/UDPClient.hpp"
#include "Headers/ModuleDesignations.hpp"
#include "MAVLink/common/mavlink.h"
#include "MAVLink/common/mavlink_msg_heartbeat.h"
#include "Headers/TCPClient.hpp"

#include "ppm_reader.hpp"
#include "motor_controller.hpp"

extern "C" {
#include <wiringPi.h>
}

const int DATA_LIMIT_SIZE = 3000;
const std::string LOOPBACK_ADDRESS = "127.0.0.1";
const std::string KEY = "InternationalSpaceForce";
std::mutex driver_mtx;

bool auto_kswitch_enabled = false;
bool auto_mode_select = false;

const unsigned int MOTOR_LOOP_TIME = 10000;	// loop time in us

motor_controller bolt_motor_controller;

ConcurrentQueue<std::vector<char>>* global_srsDataQueue;

extern float channels[10];
extern std::mutex chan_mtx;

void PrintToScreen(std::string dataToPrint) {
    std::lock_guard<std::mutex>lck(driver_mtx);
    printf("%s", dataToPrint.c_str());
}

void PrintToScreen02(char dataToPrint) {
    std::lock_guard<std::mutex>lck(driver_mtx);
    printf("%02X ", dataToPrint);
}

void PrintToScreenChar(char dataToPrint) {
    std::lock_guard<std::mutex>lck(driver_mtx);
    printf("%c ", dataToPrint);
}

std::vector<char> DecryptData(std::vector<char> encryptedData, std::string key) {
    std::vector<char> decyptedData({});

    for(int index = 0; index < encryptedData.size(); index++) {
        if (encryptedData[index] != '\n') {
            int keyModuloIndex = index % key.size();
            decyptedData.push_back((char) encryptedData[index] ^ key[keyModuloIndex]);
        }
        else
            decyptedData.push_back(encryptedData[index]);
    }

    return decyptedData;
}

std::vector<char> encryptData(std::vector<char> rawData, std::vector<char> key) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ key[index % key.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorToCharArray(std::vector<char> inputData, char* outputData) {
    if(inputData.size() <= DATA_LIMIT_SIZE) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

[[noreturn]] void SendDataToServerConnection(UDPClient& clientConnection, ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        std::vector<char> dataToSend;
        char buffer[DATA_LIMIT_SIZE];

        dataQueue.pop(dataToSend);
        VectorToCharArray(dataToSend, buffer);

        if (!clientConnection.SentDatagramToServerSuccessfully(buffer, dataToSend.size())) {
            printf("Error sending data\n");
        }
    }
}

[[noreturn]] void ReceivedDataFromServer(UDPServer& serverModuleConnection, ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        int receivedDataByteCount = 0;
        char buffer[DATA_LIMIT_SIZE];

        serverModuleConnection.GetDatagram(std::ref(receivedDataByteCount), buffer, DATA_LIMIT_SIZE);

        PrintToScreen("Received from server: \n");
        for (int i = 0; i < 15; ++i)
            PrintToScreen02(buffer[i]);
        PrintToScreen("\n");

        std::vector<char> bufferVector(buffer, buffer + sizeof(buffer)/ sizeof(char));
        std::vector<char> decryptedDataArray = DecryptData(bufferVector, KEY);

        PrintToScreen("Decrypted message: (First 15 characters)\n");
        for (int i = 0; i < 15; ++i)
            PrintToScreenChar(decryptedDataArray[i]);
        PrintToScreen("\n");
    }
}

[[noreturn]] void SendHeartbeatStatusMessage(ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        /* Send Heartbeat */
        uint8_t buf[DATA_LIMIT_SIZE];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(MotorControlModule, MotorControlModule, &msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);

        std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
        dataQueue.push(dataToSend);

        // Sleep for 10 seconds
        sleep(10);
    }
}

void ParseCmdInt(mavlink_command_int_t mavCmdInt, ConcurrentQueue<std::vector<char>>& outgoingData) {
    uint16_t msgChecksum = 0;
    float latitude = 0;
    float longitude = 0;

    switch (mavCmdInt.command) {

        case MAV_CMD_DO_FLIGHTTERMINATION:
            // Received kill switch status update
        	::auto_kswitch_enabled = (mavCmdInt.param1 == 1);
            //AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_PAUSE_CONTINUE:
            // Received go/stop cmd for mission, pass on central exec
        	::auto_kswitch_enabled = (mavCmdInt.param1 == 0);
            //AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_SET_MODE:
        	::auto_mode_select = (mavCmdInt.param1 == 64);
            //AddFutureWaitForMessage(msgChecksum);
            break;

        default:
            break;
    }
}


[[noreturn]] void ParseMessagesInQueue(ConcurrentQueue<std::vector<char>>& messagesToParse, ConcurrentQueue<std::vector<char>>& outgoingData) {

    while(true) {
    	char receivedBuffer[DATA_LIMIT_SIZE];
       	int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        messagesToParse.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();
        VectorToCharArray(dataToBeSent, receivedBuffer);

        mavlink_message_t msg;
        mavlink_status_t status;
        mavlink_heartbeat_t heartbeat;
        mavlink_rc_channels_raw_t rc_channels;
        unsigned int firstCharInBuffer = -1;

        for (int bufferIndex = 0; bufferIndex < receivedByteCount; ++bufferIndex) {
            firstCharInBuffer = dataToBeSent[bufferIndex];
            //printf("%02x ", (unsigned char) firstCharInBuffer);

            if (mavlink_parse_char(MAVLINK_COMM_0, dataToBeSent[bufferIndex], &msg, &status)) {

            	printf("Message ID: %d\n", msg.msgid);

				switch(msg.msgid){

				case MAVLINK_MSG_ID_COMMAND_INT:
				{
					mavlink_command_int_t mavCmdInt;
					ParseCmdInt(mavCmdInt, std::ref(outgoingData));
					mavlink_msg_command_int_decode(&msg, &mavCmdInt);
					uint8_t buf[DATA_LIMIT_SIZE];
					uint16_t len;

					mavlink_msg_command_ack_pack(MotorControlModule, MotorControlModule, &msg,
												   mavCmdInt.command, 0, 0, 0, mavCmdInt.target_system, mavCmdInt.target_component);

					len = mavlink_msg_to_send_buffer(buf, &msg);

					std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
					outgoingData.push(dataToSend);
				}
					break;

				default:
					break;

				}
            }
        }
    }
}

void CheckIfExpectedMsgReceived(uint16_t expectedMsgChecksum) {
/*
	int currentMsgWaitCount = messagesToBeReturnedRetryCount.GetElementAt(expectedMsgChecksum);
    if(currentMsgWaitCount == 0) {
        // Expected message was received
        RemoveMsgToBeWaitedOn(messagesToBeReturnedRetryCount, expectedMsgChecksum);
    } else {
        // Expected message was not received
        DecrementMsgWaitResponse(messagesToBeReturnedRetryCount, expectedMsgChecksum);
        FutureRun waitForMessage(5000, true, &CheckIfExpectedMsgReceived, expectedMsgChecksum);
    }
*/
}

void AddFutureWaitForMessage(uint16_t expectedMsgChecksum) {
    //AddNewMsgToWaitForResponse(messagesToBeReturnedRetryCount, expectedMsgChecksum);
    //FutureRun waitForMessage(DEFAULT_ACK_WAIT_TIME, true, &CheckIfExpectedMsgReceived, expectedMsgChecksum);
}

void MotorControlLoop(ConcurrentQueue<std::vector<char>>& srsDataQueue, ConcurrentQueue<std::vector<char>>& cameraDataQueue)
{
	int error = 0;
	unsigned int loop_timer;
	int raw_channel_values[NUM_PPM_CHANNELS];
	int final_channel_values[NUM_PPM_CHANNELS];
	states current_state = states::INVALID;
	states new_state = states::INVALID;

	uint8_t buf[DATA_LIMIT_SIZE];
	mavlink_message_t msg;
	uint16_t len;

	int manual_kill_switch = 0;

	for(int i=0; i < NUM_PPM_CHANNELS; i++)
		raw_channel_values[i] = default_values[i];

	printf("Entering Motor Control Loop!");

	while(true) // && (error == 0) !removing the error until the future
	{
		loop_timer = micros();

		chan_mtx.lock();
		raw_channel_values[0] = raw_channel_values[0]*0.2 + 0.8*channels[0];
		raw_channel_values[1] = raw_channel_values[1]*0.2 + 0.8*channels[1];
		raw_channel_values[2] = raw_channel_values[2]*0.2 + 0.8*channels[2];
		raw_channel_values[3] = raw_channel_values[3]*0.2 + 0.8*channels[3];
		raw_channel_values[4] = channels[8];
		raw_channel_values[5] = channels[5];	// manual override
		raw_channel_values[6] = channels[4];	// kill switch
		raw_channel_values[7] = channels[6];	// vacuum switch
		raw_channel_values[8] = channels[7];	// mode select
		chan_mtx.unlock();

		if((::auto_kswitch_enabled == true) || (1500 < raw_channel_values[KILL_SWITCH_CHANNEL]))
		{
			printf("kill switch\n");
			// send out the default signals to the motors!
			for(int i=0; i<10; i++){final_channel_values[i] = 1500;}

			mavlink_msg_rc_channels_raw_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, 0, 0, 1500, 1500, 1500, 1000, 1500, 1500, 1500, 1500, 255);
			len = mavlink_msg_to_send_buffer(buf, &msg);
			std::vector<char> armDataToSend = CharArrayToVector((char*)buf, len);
			srsDataQueue.push(armDataToSend);

			// TODO: turn off the rpi?
		}
		else
		{
			if(auto_mode_select == true)
			{
				printf("autonomous mode\n");
				for(int i=0; i<NUM_PWM_CHANNELS; i++)
					final_channel_values[i]=( i < 2 ? MAX_FORWARD : 1500);

			}else if(raw_channel_values[MODE_CHANNEL] < 1500)
			{
				printf("manual mode\n");
				// 1. translate motor commands
				bolt_motor_controller.translate_manual_controls(&raw_channel_values[0], &final_channel_values[0], 2);

				// 2. send stop signals to the arm module
				mavlink_msg_rc_channels_raw_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, 0, 0, 1500, 1500, 1500, 1000, 1500, 1500, 1500, 1500, 255);
				len = mavlink_msg_to_send_buffer(buf, &msg);
				std::vector<char> armDataToSend = CharArrayToVector((char*)buf, len);
				srsDataQueue.push(armDataToSend);

			}else if(1500 < raw_channel_values[MODE_CHANNEL])
			{
				printf("arm mode\n");
				// 1. send stop command to motors
				for(int i=0; i<10; i++){final_channel_values[i] = 1500;}

				// 2. send manual controls to arm module
				mavlink_msg_rc_channels_raw_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, 0, 0, raw_channel_values[2], raw_channel_values[1], raw_channel_values[0], raw_channel_values[0], raw_channel_values[VAC_CHANNEL] < 1300 ? 1000 : 2000, raw_channel_values[VAC_CHANNEL], 1500, 1500, 255);
				len = mavlink_msg_to_send_buffer(buf, &msg);
				std::vector<char> armDataToSend = CharArrayToVector((char*)buf, len);
				srsDataQueue.push(armDataToSend);
			}else
			{
				// send out stop commands to everything!
				for(int i=0; i<10; i++){final_channel_values[i] = 1500;}

				mavlink_msg_rc_channels_raw_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, 0, 0, 1500, 1500, 1500, 1000, 1500, 1500, 1500, 1500, 255);
				len = mavlink_msg_to_send_buffer(buf, &msg);
				std::vector<char> armDataToSend = CharArrayToVector((char*)buf, len);
				srsDataQueue.push(armDataToSend);
			}
		}

		// send out pwm controls...
		error = bolt_motor_controller.send_pwm_signals(mapped_channel, final_channel_values, 2);

		// send out camera controls...
		// send out stop commands to everything!
		mavlink_msg_rc_channels_raw_pack(CameraControlModule, CameraControlModule, &msg, 0, 0, raw_channel_values[CAMERA_CHANNEL], 1500, 1500, 1500, 1500, 1500, 1500, 1500, 255);
		len = mavlink_msg_to_send_buffer(buf, &msg);
		std::vector<char> cameraDataToSend = CharArrayToVector((char*)buf, len);
		cameraDataQueue.push(cameraDataToSend);

		if((micros() - loop_timer) < MOTOR_LOOP_TIME + 100)
		{
			delayMicroseconds(MOTOR_LOOP_TIME - (micros() - loop_timer));
		}
		else
		{
			fprintf(stderr,"Exceeded Motor Loop Time! %d", (micros() - loop_timer));
			error = 1;
		}
		// sleep for the rest of the time

	}

}

void ReceiverLoop()
{
    // Initialize all the good stuff
	while(true)
	{
		sleep(0.02);
	}

}

void before_exit(int signum)
{
	bolt_motor_controller.deinit();

	deinitialize_gpio();

	exit(signum);
}

void bye_bye()
{
	uint8_t buf[DATA_LIMIT_SIZE];
	mavlink_message_t msg;
	uint16_t len;

	mavlink_msg_rc_channels_raw_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, 0, 0, 1500, 1500, 1500, 1000, 1500, 1500, 1500, 1500, 1);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	std::vector<char> armDataToSend = CharArrayToVector((char*)buf, len);
	global_srsDataQueue->push(armDataToSend);
}

void handle_abort(int signum)
{
	bolt_motor_controller.deinit();

	deinitialize_gpio();

	printf("Sending abort to arm!\n");

	bye_bye();

	abort();
}

int main()
{
	// register
	signal(SIGINT, before_exit);
	signal(SIGABRT, handle_abort);


    UDPClient client(MissionExecutiveModule, "127.0.0.1");
    UDPClient srsClient(SoldierRetrievalModule, "127.0.0.1");
    UDPClient cameraClient(CameraControlModule, "127.0.0.1");
    UDPServer receiverFromServerModule(MotorControlModule, "127.0.0.1");

    ConcurrentQueue<std::vector<char>> sendDataQueue;
    ConcurrentQueue<std::vector<char>> srsDataQueue;
    ConcurrentQueue<std::vector<char>> cameraDataQueue;
    ConcurrentQueue<std::vector<char>> receivedDataQueue;

    global_srsDataQueue = &srsDataQueue;

    char loopControl = '\n';
    //do {
        //PrintToScreen("Hit Enter to continue: ");
        //loopControl = getchar();
    //} while(loopControl == '\n');

    // Initialize all the good stuff
	if(initialize_gpio())
		PrintToScreen("Error Initializing GPIO!\n");
	else
		PrintToScreen("Successfully initialized Receiver GPIO Input!\n");

	if(bolt_motor_controller.setup_pwm())
	{
		PrintToScreen("Error Starting PWM!\n");
	}
	else
	{
		PrintToScreen("PWM Startup Sucess!\n");
	}


	std::thread parseDataReceived(ParseMessagesInQueue, std::ref(receivedDataQueue), std::ref(sendDataQueue));
    std::thread sendCameraData(SendDataToServerConnection, std::ref(cameraClient), std::ref(cameraDataQueue));
    std::thread sendSRSData(SendDataToServerConnection, std::ref(srsClient), std::ref(srsDataQueue));
	std::thread sendData(SendDataToServerConnection, std::ref(client), std::ref(sendDataQueue));
    std::thread receiveData(ReceivedDataFromServer, std::ref(receiverFromServerModule), std::ref(receivedDataQueue));
    std::thread sendHeartbeatStatus(SendHeartbeatStatusMessage, std::ref(sendDataQueue));
    std::thread motorControlLoop(MotorControlLoop, std::ref(srsDataQueue), std::ref(cameraDataQueue));
    std::thread receiverLoop(ReceiverLoop);

    PrintToScreen("Outside\n");

    parseDataReceived.join();
    sendCameraData.join();
    sendSRSData.join();
    receiverLoop.join();
    motorControlLoop.join();
    sendData.join();
    receiveData.join();
    sendHeartbeatStatus.join();




    return EXIT_SUCCESS;
}
