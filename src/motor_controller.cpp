
#include "motor_controller.hpp"

int motor_controller::pi = -1;

std::mutex controller_mutex;

const float dead_band = 100/490;

motor_controller::motor_controller()
{

}

int motor_controller::setup_pwm()
{
    pi = pigpio_start(NULL, NULL);

    if (pi < 0)
    {
    	fprintf(stderr, "Error starting pigpio!\n");
    	return 1;
    }

	return 0;
}

void motor_controller::deinit()
{
	pigpio_stop(pi);
	fprintf(stderr, "detaching from pigpio!\n");
}

int motor_controller::send_pwm_signals(const int* gpio_pins, int* pwm_values, int size)
{
	int send_error = 0;

	std::lock_guard<std::mutex>lck(controller_mutex);

	for(int i = 0; i< size; i++)
	{
		if((pwm_values[i] < PWM_MAX) && (PWM_MIN < pwm_values[i]))
		{
			printf("sending pwm: %d\n", pwm_values[i]);
			send_error |= set_servo_pulsewidth(pi, gpio_pins[i], pwm_values[i]);
		}
		else
		{
			printf("Bad PWM Value: %d, sending default signal\n", pwm_values[i]);
			send_error |= set_servo_pulsewidth(pi, gpio_pins[i], 1500);
		}
	}

	return send_error;

}

void motor_controller::translate_manual_controls(int *input_array, int *translated_array, int size)
{
	// INPUT ARRAY
	// Left:				Right:
	// CH1   <----->	CH3   <----->
	//			^				 ^
	//			|				 |
	// CH2      |		CH4      |
	//          |				 |
	//          v   			 v

	// OUTPUT ARRAY
	//
	//		||			||
	//		||			||
	// CH1  ||		CH2	||
	// 		||			||
	//		||			||
	//
	// Left Tread	Right Tread
	//
	// CH3
	//	=====
	//	| O	|
	//	=====
	//  Camera Servo


	if((1450 < input_array[1]) && (input_array[1] < 1550))
	{	// then we're just purely turning
		//translated_array[0] = (int)(1500 + (input_array[0] - 1500)*MAX_TURN);
		//translated_array[1] = (int)(1500 - (input_array[0] - 1500)*MAX_TURN);
		translated_array[0] = 1500;
		translated_array[1] = 1500;
	}
	else
	{	// not terribly sophisticated... but a simple sled steering
		if(1550 < input_array[1])
		{
			translated_array[0] = 1500 < input_array[0] ? input_array[1] : input_array[1] - MAX_TURN*((1500 - input_array[0]));
			translated_array[1] = input_array[0] < 1500 ? input_array[1] : input_array[1] - MAX_TURN*((input_array[0]- 1500));
		}
		else if(input_array[1] < 1450)
		{
			translated_array[0] = 1500 < input_array[0] ? input_array[1] : input_array[1] + MAX_TURN*((1500 - input_array[0]));
			translated_array[1] = input_array[0] < 1500 ? input_array[1] : input_array[1] + MAX_TURN*((input_array[0] - 1500));
		}
		else
		{
			translated_array[0] = 1500;
			translated_array[1] = 1500;
		}
	}

}


/*
int main()
{
	int status;

	motor_controller bolt_controller;

	// GPIO setup
	gpioCfgClock(1, PI_DEFAULT_CLK_PERIPHERAL, 0);

	status = gpioInitialise();

	if(status < 0)
	{
		fprintf(stderr, "Error Initializing GPIO Register!! status: %d\n", status);
		return 1;
	}
	else
	{
		printf("GPIO Initialization success! status: %d\n", status);
	}

	bolt_controller.setup_pwm ();

	int pwm_channel_values[8] = {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};


	while(true)
	{

	for(int j=0; j < 8; j++)
	{
		pwm_channel_values[j] += 100;

		if(2000 < pwm_channel_values[j]){pwm_channel_values[j] = 1000;}
	}

	bolt_controller.send_pwm_signals(mapped_channel, pwm_channel_values, NUM_PWM_CHANNELS);

	sleep(1);

	}

	return 0;
}
*/
