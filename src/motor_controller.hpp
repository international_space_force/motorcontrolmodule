
#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

#include <pigpiod_if2.h>
#include <pigpio.h>
#include <cstdio>
#include <fstream>
#include <unistd.h>
#include <mutex>

#define NUM_PWM_CHANNELS		8

// Mapping for Channels -> GPIO PWM Pins
#define CHANNEL_1				12		// Motor 1
#define CHANNEL_2				18		// Motor 2
#define CHANNEL_3				22
#define CHANNEL_4				23
#define CHANNEL_5				24
#define CHANNEL_6				25
#define CHANNEL_7				6
#define CHANNEL_8				5

#define PWM_MIN					950
#define PWM_MAX					2050

#define MAX_FORWARD				1900
#define MAX_TURN				0.2

const int mapped_channel[NUM_PWM_CHANNELS] = {CHANNEL_1, CHANNEL_2, CHANNEL_3, CHANNEL_4, CHANNEL_5, CHANNEL_6, CHANNEL_7, CHANNEL_8};


class motor_controller
{


	public:
		static int pi;

		motor_controller();
		static int setup_pwm();
		static void deinit();
		int send_pwm_signals(const int* gpio_pins, int* pwm_values, int size);
		void translate_manual_controls(int *input_array, int *translated_array, int size);

};




#endif

