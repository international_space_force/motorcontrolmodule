

#include <pigpio.h>
#include <stdio.h>
#include <mutex>
#include <unistd.h>
#include <Navio/gpio.h>
#include "Navio/Util.h"

unsigned int SAMPLE_RATE     			= 2;	// Sample Rate in us
unsigned int GPIO_INPUT      			= 13;   // PPM Input Pin - GPIO # not Pin #
unsigned int PPM_SYNC_PULSE_LENGTH     	= 4000; // Minimum DT between frames
unsigned int NUM_PPM_CHANNELS 			= 10;   // Number of channels packed in PPM
const unsigned int KILL_SWITCH_CHANNEL	= 6;	// Kill switch on the manual controller, 0-based
const unsigned int MODE_CHANNEL			= 8;
const unsigned int VAC_CHANNEL			= 7;
const unsigned int MOVERRIDE_CHANNEL	= 5;
const unsigned int CAMERA_CHANNEL		= 3;
bool DEBUG      						= true;

std::mutex chan_mtx;

float channels[10] = {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};
float default_values[10] = {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};

unsigned int curr_channel = 0;
unsigned int toc;
unsigned int dT;

enum class states { MANUAL = 1, AUTONOMOUS = 2, INVALID = 3};
enum class control_states { ROVER = 1, ARM = 2, INVALID = 3};

void read_ppm_from_rx(int gpio, int level, uint32_t tic)
{
	if (level == 0) {
		dT = tic - toc;
		toc = tic;

		if (dT >= PPM_SYNC_PULSE_LENGTH) { // Sync
			curr_channel = 0;

			// Console output
			if (DEBUG) {
				printf("\n");
				for (int i = 0; i < NUM_PPM_CHANNELS; i++)
					printf("%4.f ", channels[i]);
			}
		}
		else
			if (curr_channel < NUM_PPM_CHANNELS)
			{
				chan_mtx.lock();

				if(curr_channel == 4 && 1500 < dT)
				{
					printf("Kill Switch Activated!... aborting\n");
					abort();
				}

				channels[curr_channel++] = dT;
				chan_mtx.unlock();
			}
	}
}

void handlerFunction( void )
{
	// set receiver_state to FAULTED
    fprintf(stderr, "timer expired! status\n");
}

int initialize_gpio()
{
	int status = 0;

    if (check_apm()) {
        return 1;
    }

	// GPIO setup
	gpioCfgClock(SAMPLE_RATE, PI_DEFAULT_CLK_PERIPHERAL, 0); /* last parameter is deprecated now */

	status = gpioInitialise();

	if(status < 0)
	{
		fprintf(stderr, "Error Initializing GPIO Register!! status: %d\n", status);
		return 1;
	}
	else
	{
		printf("GPIO Initialization success! status: %d\n", status);
	}

	gpioSetMode(GPIO_INPUT ,PI_INPUT);

	toc = gpioTick();
	gpioSetAlertFunc(GPIO_INPUT, read_ppm_from_rx);

    printf("Initialization complete starting ppm reader...");

/*
    printf("starting receiver timer\n");
    TimeTimeoutHandlerImp * timerImp = new TimeTimeoutHandlerImp;
    Timer * timer = new Timer(timerImp );

    timer->setDuration( 1 );
    timer->start();
*/
	return 0;
}

int deinitialize_gpio()
{
	gpioTerminate();
	printf("Detaching ppm reader...\n");

	return 0;
}



