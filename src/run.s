#!/bin/bash

i="0"

while [ $i -lt 3 ]
do
        sudo killall sudo ./motor_controller
        sudo killall ../../soldierretrievalmodule/robot_arm
        sudo killall sudo ../../soldierretrievalmodule/robot_arm
        sudo killall sudo ../../cameracontrolmodule/bolt_cam
        sudo killall pigpiod
        echo "launching motor control!"
        sudo ./motor_controller &
        sleep 0.1
        echo "launching bolt cam!"
        sudo ../../cameracontrolmodule/bolt_cam &
        sleep 0.1
        echo "launching bolt arm"
        sudo ../../soldierretrievalmodule/robot_arm
        sleep 5
        i=$[$i+1]
done
