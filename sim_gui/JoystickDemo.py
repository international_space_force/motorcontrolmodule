from kivy.config import Config
Config.set('graphics', 'window_state', 'maximized')
from kivy.app import App
from kivy.garden.joystick import Joystick
from kivy.core.window import Window
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout

from pymavlink import mavutil

# things for APC class
import serial
import time
import os

import threading
from threading import Timer

receiver_data_lock = threading.Lock()

Window_size = [975, 585]

NUM_CHANNELS = 8

UPDATE_TIME = 0.1

MESSAGE_TIMEOUT = 0.05

Window.size = (Window_size[0], Window_size[1])

test_config = ""

try:
    who_am_i = os.uname()
    
    if("MBP" in who_am_i[1]):
        print('configuring for Macbook Pro!')
        Window.size = (Window_size[0], Window_size[1])
        
        test_config = "MBP"
        
    elif("raspberry" in who_am_i[1]):
        print('configuring for rpi!')
        Window.size = (Window_size[0], Window_size[1])
        
        test_config = "RPI"
        
except:
    
    test_config = "NONE"
    
    pass

def SendMavlinkMessage(self):
    
    # Start a connection listening to a UDP port
#    the_connection = mavutil.mavlink_connection('udpout:192.168.2.2:4202')  # 4203
    the_connection = mavutil.mavlink_connection('udpout:127.0.0.1:4202')  # 4203

    # Wait for the first heartbeat 
    #   This sets the system and component ID of remote system for the link
    
    #the_connection.wait_heartbeat()
    #print("Heartbeat from system (system %u component %u)" % (the_connection.target_system, the_connection.target_system))
    
    #wait_conn(master)
    
    while(True):
        
        #def rc_channels_raw_send(
        #    self, 
        #    time_boot_ms, 
        #    port, 
        #    chan1_raw, 
        #    chan2_raw, 
        #    chan3_raw, 
        #    chan4_raw, 
        #    chan5_raw, 
        #    chan6_raw, 
        #    chan7_raw, 
        #    chan8_raw, 
        #    rssi, 
        #    force_mavlink1=False):
        
        try:
            
            print(the_connection.recv_match().to_dict())
            
        except:
            
            pass
        
        
        
        receiver_data_lock.acquire();
        
        try:
        
            if 1500 < channels[7]:
                
                the_connection.mav.rc_channels_raw_send(
                    0,              #time_boot_ms
                    1,            # port
                    channels[0],    # chan1_raw
                    channels[1],    # chan2_raw
                    channels[2],    # chan3_raw
                    channels[3],    # chan4_raw
                    channels[5],    # chan5_raw
                    channels[4],          # chan6_raw
                    65535,          # chan7_raw
                    65535,          # chan8_raw
                    0             # rssi
                    )
                print("sending signals")
        finally:
        
            receiver_data_lock.release()
        
        time.sleep(0.1)

class ArduinoController:
    
    def __init__(self):
        
        self.connected = False
        self.exists = False
        
        self.error = False
        
        self.time_out = 60
        
        pass
    
    def __del(self):
        
        pass
    
    def connect(self):
        
        try:
            # 5/4/2020 -> New Board also shows up as FTDI... changed to USB1 from tty_ARDUINO
            self.serial_port = serial.Serial(port='/dev/tty.usbmodem1421', baudrate=9600, parity='N', stopbits=1, bytesize=8,xonxoff=False, timeout=1)
    
            if not(self.serial_port.isOpen()):
                # something wrong with the serial and we need to exit
                
                return 1
                
                pass
            
        except:
            
            return 1
        
        
        self.connected = True
        
        return 0

    def send_controlls(self):
        
        byte_array = "$-"
        
        
        for i in range(0, NUM_CHANNELS):
            #byte_array.append(channels[i] & 0b11111111)
            #byte_array.append(channels[i] >> 8 & 0b11111111)
            
            byte_array = byte_array + str(channels[i]) + "-"
        
        byte_array = byte_array + "*"
            
        #byte_array.append(ord('*'))
        
        str_byte_array = byte_array.encode()
        
        print('sending controlls!')
        print(str_byte_array)
        
        self.serial_port.write(byte_array.encode())
        
        start_time = time.time()
        
        while(time.time() < start_time + MESSAGE_TIMEOUT):
            
            if 0 < self.serial_port.in_waiting:
                line = self.serial_port.readline()
                list_line = list(line)
                print(line)
                
                self.error = False
                
                for i in range(0, len(line)):
                    if(not(line[i] == str_byte_array[i])):
                        self.error = True
                        break
        
                if not(self.error):
                    print('control confirmed!')
                    print('CH1: {:4d} CH2: {:4d} CH3: {:4d} CH4: {:4d} CH5: {:4d}'.format(channels[0], channels[1], channels[2], channels[3], channels[4], channels[5]))
                    
                break
            
        
        if self.connected == True:
            
            self.send_signal = Timer(UPDATE_TIME, self.send_controlls)
            
            self.send_signal.start()
        
        pass

class JoystickDemo(FloatLayout):
  pass

class DemoSwitch(BoxLayout):
    
    def __init__(self, **kwargs):
        super(DemoSwitch,self).__init__(**kwargs)
        
        self.active = False 
    
    def activate_kill_switch(self, instance):
        
        if self.active == True:
            self.active = False
            
            if instance.parent.text == "reverse":
                
                receiver_data_lock.acquire()
                try:
                    channels[4] = 1000
                    self.parent.parent.parent.parent.parent.ids.reverse_label.text = "CH5-REV: {:4d}".format(channels[4])
                finally:
                    receiver_data_lock.release()
                    
            elif instance.parent.text == "vacuum":
                
                receiver_data_lock.acquire()
                try:
                    channels[5] = 1000
                    self.parent.parent.parent.parent.parent.ids.vacuum_label.text = "CH6-VAC: {:4d}".format(channels[5])
                finally:
                    receiver_data_lock.release()
                
            elif instance.parent.text == "kill_switch":
                
                receiver_data_lock.acquire()
                try:
                    channels[6] = 1000
                    self.parent.parent.parent.parent.parent.ids.kill_switch_label.text = "CH7-KIL: {:4d}".format(channels[6])
                    old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                    mode_state = old_text[0][6:]
                    self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: {}\nKill Switch: Off".format(mode_state)
                finally:
                    receiver_data_lock.release()
                    
            elif instance.parent.text == "mode_select":
                
                receiver_data_lock.acquire()
                try:
                    channels[7] = 1000
                    self.parent.parent.parent.parent.parent.ids.mode_select_label.text = "CH8-MOD: {:4d}".format(channels[7])
                    old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                    kill_switch_state = old_text[1][13:]
                    self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: Normal\nKill Switch: {}".format(kill_switch_state)
                finally:
                    receiver_data_lock.release()
                    
            instance.background_down = 'images/switch_off.png'
            instance.background_normal = 'images/switch_off.png'
            print('de-activating kill switch!')
        else:
            self.active = True
            
            if instance.parent.text == "reverse":
                
                receiver_data_lock.acquire()
                try:
                    channels[4] = 2000
                    self.parent.parent.parent.parent.parent.ids.reverse_label.text = "CH5-REV: {:4d}".format(channels[4])
                finally:
                    receiver_data_lock.release()
                    
            elif instance.parent.text == "vacuum":
                
                receiver_data_lock.acquire()
                try:
                    channels[5] = 2000
                    self.parent.parent.parent.parent.parent.ids.vacuum_label.text = "CH6-VAC: {:4d}".format(channels[5])
                finally:
                    receiver_data_lock.release()
                    
            elif instance.parent.text == "kill_switch":
                
                receiver_data_lock.acquire()
                try:
                    channels[6] = 2000
                    self.parent.parent.parent.parent.parent.ids.kill_switch_label.text = "CH7-KIL: {:4d}".format(channels[6])
                    old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                    mode_state = old_text[0][6:]
                    self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: {}\nKill Switch: On".format(mode_state)
                finally:
                    receiver_data_lock.release()
                    
            elif instance.parent.text == "mode_select":
                
                receiver_data_lock.acquire()
                try:
                    channels[7] = 2000
                    self.parent.parent.parent.parent.parent.ids.mode_select_label.text = "CH8-MOD: {:4d}".format(channels[7])
                    old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                    kill_switch_state = old_text[1][13:]
                    self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: Arm Control\nKill Switch: {}".format(kill_switch_state)
                finally:    
                    receiver_data_lock.release()
                    
            instance.background_down = 'images/switch_on.png'
            instance.background_normal = 'images/switch_on.png'
            print('activating kill switch!')
        pass

class JoystickDemoApp(App):
    
  def build(self):
    self.root = JoystickDemo()
    self._bind_joysticks()

  def _bind_joysticks(self):
    joysticks = self._get_joysticks(self.root)
    #for joystick in joysticks:
    #  joystick.bind(pad=self._update_pad_display)
    
    joysticks[0].bind(pad=self._update_left_pad_display)
    joysticks[1].bind(pad=self._update_right_pad_display)
    

  def _get_joysticks(self, parent):
    joysticks = []
    if isinstance(parent, Joystick):
      joysticks.append(parent)
    elif hasattr(parent, 'children'):
      for child in parent.children:
        joysticks.extend(self._get_joysticks(child))
    return joysticks

  def _update_left_pad_display(self, instance, pad):
    x, y = pad
    #x, y = (str(x)[0:5], str(y)[0:5])
    #x, y = (('x: ' + x), ('\ny: ' + y))
     
    receiver_data_lock.acquire()
    try:            
        channels[2] = int(1500 + 500*x)
        channels[3] = int(1500 + 500*y)
        self.root.ids.pad_display_xy_1.text = "CH3: {:4d}".format(channels[2])
        self.root.ids.pad_display_rma_1.text = "CH4: {:4d}".format(channels[3])
    finally:
        receiver_data_lock.release()

  def _update_right_pad_display(self, instance, pad):
    x, y = pad

    receiver_data_lock.acquire()
    try:
        channels[0] = int(1500 + 500*x)
        channels[1] = int(1500 + 500*y)    
        self.root.ids.pad_display_xy_2.text = "CH1: {:4d}".format(channels[0])
        self.root.ids.pad_display_rma_2.text = "CH2: {:4d}".format(channels[1])
    finally:
        receiver_data_lock.release()


if __name__ == "__main__":
    
    channels = [1500 for i in range(0, NUM_CHANNELS)]
    
    #if test_config == "MBP":
    #    
    #    arduino_controller = ArduinoController()
    #
    #    if not(arduino_controller.connect()):
    #        print('successfully connected to Arduino PPM Generator!')
    #        
    #        time.sleep(2)
    #        
    #        arduino_controller.send_controlls()
    #    else:
    #        print('failed to connect to Arduino PPM Generator!')

    #else:
        
    x = threading.Thread(target=SendMavlinkMessage, args=(1,))
        
    x.start()
        
    #    pass

    JoystickDemoApp().run()


