

#include <pigpio.h>
#include <stdio.h>
#include <unistd.h>

#include <Navio/gpio.h>
#include "Navio/Util.h"

//=============================================================================
//=============================    SETUP   ====================================
//=============================================================================

// CONFIGURATION VAIRABLES

#define	SAMPLE_RATE				1
// SAMPLE_RATE: the sampling rate in us for the gpio pin

#define GPIO_INPUT_PIN			4
// GPIO_INPUT_PIN: GPIO Pin for reading PPM signal (refer to RPi pinout)

#define SYNC_PULSE_LEN			4000
// SYNC_PULSE_LEN: length between end of one frame and the beginning of another

#define NUM_PPM_CHANNELS		8
// NUM_PPM_CHANNELS: number of ppm channels embedded in the frame

#define	DEBUG					true
// DEBUG: use this for printing out the channel values


float channels[NUM_PPM_CHANNELS];


unsigned int curr_channel = 0;
unsigned int tic;
unsigned int dT;

//=============================================================================
//==========================  END  SETUP   ====================================
//=============================================================================


void read_ppm(int gpio, int level, uint32_t toc)
{
	if (level == 0) {	
		dT = toc - tic;
		tic = toc;
	
		if (dT >= SYNC_PULSE_LEN) { // Sync
			curr_channel = 0;

			if (DEBUG) {
				printf("\n");
				for (int i = 0; i < NUM_PPM_CHANNELS; i++)
					printf("%4.f ", channels[i]);
			}
		}
		else
			if (curr_channel < NUM_PPM_CHANNELS)
				channels[curr_channel++] = dT;
	}
}

using namespace Navio;

int main(int argc, char *argv[])
{

    if (check_apm()) {
        return 1;
    }

	// GPIO setup
	gpioCfgClock(SAMPLE_RATE, PI_DEFAULT_CLK_PERIPHERAL, 0); /* last parameter is deprecated now */

	gpioInitialise();

	gpioSetMode(GPIO_INPUT_PIN ,PI_INPUT);

	tic = gpioTick();
	gpioSetAlertFunc(GPIO_INPUT_PIN, read_ppm);

	while(1)
		sleep(10);


	return 0;
}
