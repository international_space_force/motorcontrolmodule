from kivy.config import Config
Config.set('graphics', 'window_state', 'maximized')
from kivy.app import App
from kivy.garden.joystick import Joystick
from kivy.core.window import Window
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout

# things for APC class
import serial
import time

from threading import Timer

Window_size = [975, 585]

NUM_CHANNELS = 8

UPDATE_TIME = 0.1

MESSAGE_TIMEOUT = 0.1

Window.size = (Window_size[0], Window_size[1])

class ArduinoController:
    
    def __init__(self):
        
        self.connected = False
        self.exists = False
        
        self.error = False
        
        self.time_out = 60
        
        pass
    
    def __del(self):
        
        pass
    
    def connect(self):
        
        try:
            # 5/4/2020 -> New Board also shows up as FTDI... changed to USB1 from tty_ARDUINO
            self.serial_port = serial.Serial(port='/dev/tty.usbmodem1421', baudrate=9600, parity='N', stopbits=1, bytesize=8,xonxoff=False, timeout=1)
    
            if not(self.serial_port.isOpen()):
                # something wrong with the serial and we need to exit
                
                return 1
                
                pass
            
        except:
            
            return 1
        
        
        self.connected = True
        
        return 0

    def send_controlls(self):
        
        byte_array = "$-"
        
        
        for i in range(0, NUM_CHANNELS):
            #byte_array.append(channels[i] & 0b11111111)
            #byte_array.append(channels[i] >> 8 & 0b11111111)
            
            byte_array = byte_array + str(channels[i]) + "-"
        
        byte_array = byte_array + "*"
            
        #byte_array.append(ord('*'))
        
        str_byte_array = byte_array.encode()
        
        print('sending controlls!')
        print(str_byte_array)
        
        self.serial_port.write(byte_array.encode())
        
        start_time = time.time()
        
        while(time.time() < start_time + MESSAGE_TIMEOUT):
            
            if 0 < self.serial_port.in_waiting:
                line = self.serial_port.readline()
                list_line = list(line)
                print(line)
                
                self.error = False
                
                for i in range(0, len(line)):
                    if(not(line[i] == str_byte_array[i])):
                        self.error = True
                        break
        
                if not(self.error):
                    print('control confirmed!')
                    print('CH1: {:4d} CH2: {:4d} CH3: {:4d} CH4: {:4d} CH5: {:4d}'.format(channels[0], channels[1], channels[2], channels[3], channels[4], channels[5]))
            
        
        if self.connected == True:
            
            self.send_signal = Timer(UPDATE_TIME, self.send_controlls)
            
            self.send_signal.start()
        
        pass

class JoystickDemo(FloatLayout):
  pass

class DemoSwitch(BoxLayout):
    
    def __init__(self, **kwargs):
        super(DemoSwitch,self).__init__(**kwargs)
        
        self.active = False 
    
    def activate_kill_switch(self, instance):
        
        if self.active == True:
            self.active = False
            
            if instance.parent.text == "reverse":
                
                channels[4] = 1000
                self.parent.parent.parent.parent.parent.ids.reverse_label.text = "CH5-REV: {:4d}".format(channels[4])
            
            elif instance.parent.text == "vacuum":
                
                channels[5] = 1000
                self.parent.parent.parent.parent.parent.ids.vacuum_label.text = "CH6-VAC: {:4d}".format(channels[5])
                
            elif instance.parent.text == "kill_switch":
                
                channels[6] = 1000
                self.parent.parent.parent.parent.parent.ids.kill_switch_label.text = "CH7-KIL: {:4d}".format(channels[6])
                old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                mode_state = old_text[0][6:]
                self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: {}\nKill Switch: Off".format(mode_state)
                
            elif instance.parent.text == "mode_select":
                
                channels[7] = 1000
                self.parent.parent.parent.parent.parent.ids.mode_select_label.text = "CH8-MOD: {:4d}".format(channels[7])
                old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                kill_switch_state = old_text[1][13:]
                self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: Normal\nKill Switch: {}".format(kill_switch_state)
                
            instance.background_down = 'images/switch_off.png'
            instance.background_normal = 'images/switch_off.png'
            print('de-activating kill switch!')
        else:
            self.active = True
            
            if instance.parent.text == "reverse":
                
                channels[4] = 2000
                self.parent.parent.parent.parent.parent.ids.reverse_label.text = "CH5-REV: {:4d}".format(channels[4])
            
            elif instance.parent.text == "vacuum":
                
                channels[5] = 2000
                self.parent.parent.parent.parent.parent.ids.vacuum_label.text = "CH6-VAC: {:4d}".format(channels[5])
                
            elif instance.parent.text == "kill_switch":
                
                channels[6] = 2000
                self.parent.parent.parent.parent.parent.ids.kill_switch_label.text = "CH7-KIL: {:4d}".format(channels[6])
                old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                mode_state = old_text[0][6:]
                self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: {}\nKill Switch: On".format(mode_state)
                
            elif instance.parent.text == "mode_select":
                
                channels[7] = 2000
                self.parent.parent.parent.parent.parent.ids.mode_select_label.text = "CH8-MOD: {:4d}".format(channels[7])
                old_text = self.parent.parent.parent.parent.parent.ids.mode_label.text.splitlines()
                kill_switch_state = old_text[1][13:]
                self.parent.parent.parent.parent.parent.ids.mode_label.text = "Mode: Arm Control\nKill Switch: {}".format(kill_switch_state)
                
            instance.background_down = 'images/switch_on.png'
            instance.background_normal = 'images/switch_on.png'
            print('activating kill switch!')
        pass

class JoystickDemoApp(App):
    
  def build(self):
    self.root = JoystickDemo()
    self._bind_joysticks()

  def _bind_joysticks(self):
    joysticks = self._get_joysticks(self.root)
    #for joystick in joysticks:
    #  joystick.bind(pad=self._update_pad_display)
    
    joysticks[0].bind(pad=self._update_left_pad_display)
    joysticks[1].bind(pad=self._update_right_pad_display)
    

  def _get_joysticks(self, parent):
    joysticks = []
    if isinstance(parent, Joystick):
      joysticks.append(parent)
    elif hasattr(parent, 'children'):
      for child in parent.children:
        joysticks.extend(self._get_joysticks(child))
    return joysticks

  def _update_left_pad_display(self, instance, pad):
    x, y = pad
    #x, y = (str(x)[0:5], str(y)[0:5])
    #x, y = (('x: ' + x), ('\ny: ' + y))
     
    channels[2] = int(1500 + 500*x)
    channels[3] = int(1500 + 500*y)
    self.root.ids.pad_display_xy_1.text = "CH3: {:4d}".format(channels[2])
    self.root.ids.pad_display_rma_1.text = "CH4: {:4d}".format(channels[3])

  def _update_right_pad_display(self, instance, pad):
    x, y = pad

    channels[0] = int(1500 + 500*x)
    channels[1] = int(1500 + 500*y)    
    self.root.ids.pad_display_xy_2.text = "CH1: {:4d}".format(channels[0])
    self.root.ids.pad_display_rma_2.text = "CH2: {:4d}".format(channels[1])



if __name__ == "__main__":
    
    channels = [1500 for i in range(0, NUM_CHANNELS)]
    
    arduino_controller = ArduinoController()
    
    if not(arduino_controller.connect()):
        print('successfully connected to Arduino PPM Generator!')
        
        time.sleep(2)
        
        arduino_controller.send_controlls()
    else:
        print('failed to connect to Arduino PPM Generator!')

    JoystickDemoApp().run()


