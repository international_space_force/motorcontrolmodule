
from kivy.app import App
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.graphics import Color, Rectangle, RoundedRectangle
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.switch import Switch

# things for APC class
import serial
import time

Window_size = [500, 300]

Window.size = (Window_size[0], Window_size[1])

class ArduinoController:
    
    def __init__(self):
        
        self.connected = False
        self.exists = False
        
        self.is_temp = False
        self.is_ato = False
        
        self.temperature = 0
        # the level reading for each of the sensors and then the last time it was filled
        self.ato = [ 0, 0, 0, 0]
        
        self.time_out = 60
        
        pass
    
    def __del(self):
        
        pass
    
    def connect(self):
        
        try:
            # 5/4/2020 -> New Board also shows up as FTDI... changed to USB1 from tty_ARDUINO
            self.serial_port = serial.Serial(port='/dev/tty.usbmodem1421', baudrate=9600, parity='N', stopbits=1, bytesize=8,xonxoff=False, timeout=1)
    
            if not(self.serial_port.isOpen()):
                # something wrong with the serial and we need to exit
                
                return 1
                
                pass
            
        except:
            
            return 1
        
        
        self.connected = True
        
        return 0
    
    def disconnect(self):
        
        if self.serial_port.isOpen():
            self.serial_port.close()
            
        self.connected = False
    
    def is_temp(self):
        
        return self.is_temp

    def set_temp(self, on_off):
        
        self.is_temp = on_off
    
    def get_temp(self):
        
        if self.is_temp:
            
            return self.temperature
        
        else:
            
            return 0

    def is_ato(self):
        
        return self.is_ato
        
    def set_ato(self, on_off):
        
        self.ato = on_off
        
    def get_ato(self):
        
        if self.is_ato:
        
            return self.ato
        
        else:
            
            return 0
        
    def read_sensor(self):
        
        if self.connected:
            
            print('reading new sensor values')
            
            self.serial_port.flush()
            
            # start countdown timer
            start_time=time.time()
            
            # wait for response from Arduino
            while (time.time() - start_time) < self.time_out:
                
                if (self.serial_port.in_waiting > 0):
                    #line = self.serial_port.readline().decode().strip()
                    line = self.serial_port.readline()
                    
                    if (line[0] == 36) and (10 <= len(line)):
                        
                        self.ato[0] = line[1] << 8
                        self.ato[0] = self.ato[0] | line[2]
                        self.ato[1] = line[3] << 8
                        self.ato[1] = self.ato[1] | line[4]
                        self.ato[2] = line[5] << 8
                        self.ato[2] = self.ato[2] | line[6]
                        self.ato[3] = line[7] << 8
                        self.ato[3] = self.ato[3] | line[8]
                    
                    print("Tank Low: {}\nTank High: {}".format(self.ato[0], self.ato[2]))
                    
                    #parse line info.
                    return 1
        
        else:
            
            return 0
        
        
        return 0

class MainScreen(Screen):
    
    pass
    
class MainApp(App):
    
    def build(self):  
        
        widget = MainScreen()
        
        return widget

if __name__ == "__main__":
    
    arduino_controller = ArduinoController()
    
    if arduino_controller.connect():
        print('successfully connected to Arduino Controller!')
        
    MainApp().run()  
