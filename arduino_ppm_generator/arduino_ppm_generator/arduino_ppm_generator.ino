

//////////////////////CONFIGURATION///////////////////////////////
#define CHANNEL_NUMBER 8  //set the number of chanels
#define CHANNEL_DEFAULT_VALUE 1500  //set the default servo value
#define FRAME_LENGTH 22500  //set the PPM frame length in microseconds (1ms = 1000µs)
#define PULSE_LENGTH 300  //set the pulse length
#define onState 1  //set polarity of the pulses: 1 is positive, 0 is negative
#define sigPin 10  //set PPM signal output pin on the arduino

/*this array holds the servo values for the ppm signal
 change theese values in your code (usually servo values move between 1000 and 2000)*/
int ppm[CHANNEL_NUMBER];
int ppm_[CHANNEL_NUMBER];

int counter = 0;
int which_channel = 0;
bool new_message = false;
char message[80];

int error = 0;

void setup(){  

  Serial.begin(9600);

  //initiallize default ppm values
  for(int i=0; i<CHANNEL_NUMBER; i++){
      ppm[i]= CHANNEL_DEFAULT_VALUE;
  }

  pinMode(sigPin, OUTPUT);
  digitalWrite(sigPin, !onState);  //set the PPM signal pin to the default state (off)
  
  cli();
  TCCR1A = 0; // set entire TCCR1 register to 0
  TCCR1B = 0;
  
  OCR1A = 100;  // compare match register, change this
  TCCR1B |= (1 << WGM12);  // turn on CTC mode
  TCCR1B |= (1 << CS11);  // 8 prescaler: 0,5 microseconds at 16mhz
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
  sei();

}

void loop(){

  while(Serial.available())
  {
    byte new_byte = Serial.read();

    message[counter] = new_byte;
    
    counter++;
  
    if(new_byte == '*')
    {
      new_message = true;
      which_channel = 0;

      message[counter+1] = '\0';

      if(error == 1)
      {
        error = 2;
      }
      
      break;
    }
    
    if(new_byte == '$')
    {
      which_channel = 0;
      message[0] = '$';
      counter = 1;
      error = 1;
      for(int j=0; j < CHANNEL_NUMBER; j++){ppm_[j] = 0;}
    }

    if(new_byte == '-')
    {
      which_channel++;
    }

    if('0' <= new_byte && new_byte <= '9')
    {
      ppm_[which_channel - 1] = ppm_[which_channel - 1]*10 + new_byte - '0';
    }
    
  }

  if(new_message == true)
  {
    Serial.flush();
    
    if(error == 2)
    {
      Serial.print("$-");
      
      for(int i=0; i < CHANNEL_NUMBER; i++)
      {
        if(!(1000 <= ppm_[i] && ppm_[i] <= 2000))
        {
          ppm[i] = 1500;
          Serial.print(1500);
        }else
        {
          ppm[i] = ppm_[i];
          Serial.print(ppm[i]);
        }

        Serial.print("-");
      }

      Serial.print("*");

    }

    error = 0;
    
    new_message = false;
  }
  
}

ISR(TIMER1_COMPA_vect){  //leave this alone
  static boolean state = true;
  
  TCNT1 = 0;
  
  if (state) {  //start pulse
    digitalWrite(sigPin, onState);
    OCR1A = PULSE_LENGTH * 2;
    state = false;
  } else{  //end pulse and calculate when to start the next pulse
    static byte cur_chan_numb;
    static unsigned int calc_rest;
  
    digitalWrite(sigPin, !onState);
    state = true;

    if(cur_chan_numb >= CHANNEL_NUMBER){
      cur_chan_numb = 0;
      calc_rest = calc_rest + PULSE_LENGTH;// 
      OCR1A = (FRAME_LENGTH - calc_rest) * 2;
      calc_rest = 0;
    }
    else{
      OCR1A = (ppm[cur_chan_numb] - PULSE_LENGTH) * 2;
      calc_rest = calc_rest + ppm[cur_chan_numb];
      cur_chan_numb++;
    }     
  }
}
